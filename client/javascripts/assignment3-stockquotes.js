/*global app */
/* CRIA-WT Manuel Brand 474283 2/4/2015 */
var stockquotes = window.app, io, data;

(function () {
    "use strict";

    window.app = {
        series: {},

        settings: {
            refresh: 1000,
            url: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php",
            socket: io("http://server7.tezzt.nl:1333"),

            // NOTE: Use dataRetrieve to change data input
            dataRetrieve: "Socket" // Change between: Test / Ajax / Socket
        },

        parseData: function (rows) {
            var i, company;
            for (i = 0; i < rows.length; i += 1) {
                company = rows[i].col0;
                if (app.series[company] !== undefined) {
                    app.series[company].push(rows[i]);
                } else {
                    app.series[company] = [rows[i]];
                }
            }
        },

        retrieveData: function () {
            var xhr;
            xhr = new XMLHttpRequest();
            xhr.open("GET", app.settings.url, true);
            xhr.addEventListener("load", app.getJSONString);
            xhr.send();
        },

        getJSONString: function (e) {
            var response = e.target.responseText, obj;
            obj = JSON.parse(response);
            app.parseData(obj.query.results.row);
        },

        colorRows: function (col, row) {
            if (col > 0) {
                row.className = "up";
            } else if (col < 0) {
                row.className = "down";
            } else {
                row.className = "nochange";
            }
        },

        createValidCompanyName: function (str) {
            return str.replace(/[^a-zA-Z_0-9]/g, "");
        },

        generateRandom: function (min, max) {
            var temp;
            temp = Math.random() * (max - min + 1) + min;
            temp = Math.round(temp * 100) / 100;
            return temp;
        },

        getFormattedDate: function (date) {
            var newDate, month, day, year;
            month = parseInt(date.getMonth(), 10) + 1;
            day = parseInt(date.getDate(), 10);
            year = date.getFullYear().toString();
            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }
            newDate = month + "/" + day + "/" + year;
            return newDate;
        },

        getFormattedTime: function (date) {
            var time, hr, min;
            hr = date.getHours().toFixed();
            min = date.getMinutes().toString();
            if (hr.length > 1) {
                if (min.length < 2) {
                    min = "0" + min.toString();
                }
                time = (hr - 12).toString() + ":" + min + " pm";
            } else {
                time = hr.toString() + ":" + min + " am";
            }
            return time;
        },

        getRealTimeData: function () {
            app.settings.socket.on("stockquotes", function (data) {
                app.parseData(data.query.results.row);
                app.replaceTable();
            });
        },

        generateTestData: function () {
            var company, quote, newQuote, date;
            date = new Date();

            for (company in app.series) {
                if (app.series.hasOwnProperty(company)) {
                    quote = app.series[company][0];
                    newQuote = Object.create(quote);
                    newQuote.col0 = company;
                    newQuote.col2 = app.getFormattedDate(date);
                    newQuote.col3 = app.getFormattedTime(date);
                    newQuote.col4 = app.generateRandom(-10, 10);
                    newQuote.col5 = app.generateRandom(-10, 1000);
                    newQuote.col6 = app.generateRandom(-10, 1000);
                    newQuote.col7 = "N/A";
                    newQuote.col8 = "NaN";
                    app.series[company].push(newQuote);
                }
            }
        },

        showData: function () {
            var company, table, header, row, cell, quote, propertyName, propertyValue;
            table = document.createElement("table");
            header =  table.createTHead();
            row = header.insertRow(0);
            cell = row.insertCell(0);
            cell.innerHTML = "Company";
            cell = row.insertCell(1);
            cell.innerHTML = "Date";
            cell = row.insertCell(2);
            cell.innerHTML = "Time";
            cell = row.insertCell(3);
            cell.innerHTML = "Price";
            cell = row.insertCell(4);
            cell.innerHTML = "Difference";
            cell = row.insertCell(5);
            cell.innerHTML = "Average";
            cell = row.insertCell(6);
            cell.innerHTML = "High";
            cell = row.insertCell(7);
            cell.innerHTML = "Low";
            cell = row.insertCell(8);
            cell.innerHTML = "Volume";

            for (company in app.series) {
                if (app.series.hasOwnProperty(company)) {
                    quote = app.series[company][app.series[company].length - 1];
                    row = document.createElement("tr");
                    row.id = app.createValidCompanyName(company);
                    app.colorRows(quote.col4, row);

                    table.appendChild(row);
                    for (propertyName in quote) {
                        if (quote.hasOwnProperty(propertyName)) {
                            propertyValue = quote[propertyName];
                            cell = document.createElement("td");
                            cell.innerText = propertyValue;
                            row.appendChild(cell);
                        }
                    }
                }
            }

            return table;
        },

        loop: function () {
            if (app.settings.dataRetrieve === "Ajax") {
                app.retrieveData();
            }
            if (app.settings.dataRetrieve === "Test") {
                app.generateTestData();
            }
            app.replaceTable();
            setTimeout(app.loop, app.settings.refresh);
        },

        replaceTable: function () {
            var container;
            container = document.querySelector("#container");
            document.querySelector("#container").removeChild(document.querySelector("table"));
            container.appendChild(app.showData());
        },

        initHTML: function () {
            var container, h1Node;
            container = document.createElement("div");
            container.id = "container";
            app.container = container;
            h1Node = document.createElement("h1");
            h1Node.innerText = "Real time Stockquotes app";
            app.container.appendChild(h1Node);
            return app.container;
        },

        init: function () {
            var container, bodyNode;
            bodyNode = document.querySelector("body");
            container = app.initHTML();
            bodyNode.appendChild(container);

            if (app.settings.dataRetrieve === "Test") {
                app.parseData(data.query.results.row);
            }
            container.appendChild(app.showData());

            if (app.settings.dataRetrieve === "Socket") {
                app.getRealTimeData();
            }
            if (app.settings !== "Socket") {
                app.loop();
            }
        }
    };
}());