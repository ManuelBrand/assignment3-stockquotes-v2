Document all concepts and your implementation decisions.


#Flow of the program
1. init
2. retrieve data
3. draw data
4. go to step 2


#Concepts
For every concept the following items:
- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritive and authentic)

###Objects, including object creation and inheritance
Object are
There are multiple object creation methods:

var person1 = new Object();
person1.name = "Jaap";
person1.age = 57;

var person2 = {
    name: “Joop”,
    age: 65,
    job: “Garbageman”,
    sayName: function () {
        alert(this.name);
    }
};


Factory pattern:
function createPerson (name, age, job) {
    var o = new Object();
    o.name = name;
    o.age = age;
    o.job = job;
    o.sayName = function () {
        alert(this.name);
    };

    return o;
}

var person3 = createPerson("Henkie", 41, "Garbageman");


Constructor pattern:
function Person (name, age, job) {
    this.name = name;
    this.age = age;
    this.job = job;
    this.sayName = function () {
        alert(this.name);
    };
}

var person4 = new Person("Henkie", 41, "Garbageman");


Prototype pattern:
function Person () {

}

Person.prototype.name = "Henkie";
Person.prototype.age = 41;
Person.prototype.job = "Garbageman";
Person.prototype.sayName = function () {
    alert(this.name);
};

var p1 = new Person();
p1.name = "Henkie";
p1.age = 28;


Inheritance:
function SuperType () {
    this.property = true;
}

SuperType.prototype.getSuperValue = function () {
    return this.property;
};

function SubType() {
    this.subproperty = false;
}

//inherit from SuperType
SubType.prototype = new SuperType();
SubType.prototype.getSubValue = function () {
    return this.subproperty;
};

var instance = new SubType();
alert(instance.getSuperValue()); //true


Professional JavaScript for Web Developers, Nicholas C. Zakas, Chapter 6: Object Oriented Programming.
Introduction to Object-Oriented JavaScript, https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript?redirectlocale=en-US&redirectslug=JavaScript%2FIntroduction_to_Object-Oriented_JavaScript

###websockets
Websockets are a way to establish communication between client and server.
The client can send requests to the server and the server can send data to the client without a request from the client.
The client can recieve data, and gets notified when it does so.

getRealTimeData: function() {
    stockquotes.socket.on("stockquotes", function(data) {
        stockquotes.parseData(data.query.results.row);
        stockquotes.replaceTable();
    });
}


JavaScript Programming, Jon Raasch, Part III Chapter 9: Going Real-Time With WebSockets.

###XMLHttpRequest
XMLHttpRequest provides an easy way to retrieve data from a URL without having to do a full page refresh.
A Web page can update just a part of the page without disrupting what the user is doing.

retrieveData: function () {
    var xhr;

    xhr = new XMLHttpRequest();
    xhr.open("GET", stockquotes.settings.ajaxUrl, true);
    xhr.addEventListener("load", stockquotes.getJSONString);
    xhr.send();
}


Professional JavaScript for Web Developers, Nicholas C. Zakas, Chapter 21: Ajax and Comet.
XMLHttpRequest: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest?redirectlocale=en-US&redirectslug=DOM%2FXMLHttpRequest

###AJAX
With Ajax, web applications can send data to and retrieve from a server asynchronously (in the background)
without interfering with the display and behavior of the existing page.

// Initialize the Ajax request.
var xhr = new XMLHttpRequest();
xhr.open('get', 'send-ajax-data.php');

// Track the state changes of the request.
xhr.onreadystatechange = function () {
    var DONE = 4; // readyState 4 means the request is done.
    var OK = 200; // status 200 is a successful return.
    if (xhr.readyState === DONE) {
        if (xhr.status === OK) {
            alert(xhr.responseText); // 'This is the returned text.'
        } else {
            alert('Error: ' + xhr.status); // An error occurred during the request.
        }
    }
};

// Send the request to send-ajax-data.php
xhr.send(null);


AJAX in programming: http://en.wikipedia.org/wiki/Ajax_(programming)

###Callbacks
A callback is a function that takes a pointer to another function (or itself, making it recursive) as a parameter.

function callbackTest (c) {
    c();
}

function c () {
    alert("test succeeded");
}


Callbacks: https://developer.mozilla.org/en-US/docs/Mozilla/js-ctypes/js-ctypes_reference/Callbacks

###How to write testable code for unit-tests
For this project, make sure to use the testdata to run the tests.

Write independant functions which compare the expected results of the function it is supposed to check,
to the actual result of the checked function.

// removes special characters from given string
createValidCSSNameFromCompany: function(str) {
    return str.replace(/[^a-zA-Z_0-9-]/g, "");
}

// unit test for createValidCSSNameFromCompany
it("createValidCSSNameFromCompany: only letters and numbers should remain", function () {
    expect(stockquotes.createValidCSSNameFromCompany("HGF>&#$%6*")).toBe("HGF6");
});


Unit Testing tutorial: https://developer.mozilla.org/en-US/Add-ons/SDK/Tutorials/Unit_testing