/*jslint browser: true, plusplus:true*/

var stockquotes = window.app;

describe("scenario", function () {

    it("init should work", function () {
        expect(app.init()).toBe(undefined);
    });

    it("Dom elements will be created", function () {
        expect(app.initHTML().querySelector("h1").innerText).toBe("Real time Stockquotes app");
    });

    it("series cannot be empty", function () {
        expect(app.series).not.toBe({});
    });

    it("table has sufficient rows", function () {
        app.parseData(data.query.results.row);
        expect(app.showData().querySelectorAll("tr").length).toBe(26);
    });

    it("companyname is always valid", function () {
        expect(app.createValidCompanyName("HGF>&#$%6*")).toBe("HGF6");
    });

    it("prasedata tests data.js", function () {
        app.parseData(data.query.results.row[0]);
        expect(JSON.stringify(app.series["BCS"][0])).toBe(
            JSON.stringify({
                col0: "BCS",
                col1: "18.29",
                col2: "03\/09\/2015",
                col3: "10:29am",
                col4: "0.87",
                col5: "N\/A",
                col6: "N\/A",
                col7: "N\/A",
                col8: "8900"
            })
        );
    });

    it("getJSONString should get JSON string from argument and call parseData to fill series with the string", function () {
        var e = {
            target: {
                responseText: '{ "query": { "results": { "row": [{"col0": "BCS","col1": "18.29", "col2": "03\/09\/2015", "col3": "10:29am", "col4": "0.87","col5": "N\/A", "col6": "N\/A", "col7": "N\/A", "col8": "8900"}]}} }'
            }
        };
        app.getJSONString(e);
        expect(JSON.stringify(app.series["BCS"][0])).toBe(
            JSON.stringify({
                col0: "BCS",
                col1: "18.29",
                col2: "03\/09\/2015",
                col3: "10:29am",
                col4: "0.87",
                col5: "N\/A",
                col6: "N\/A",
                col7: "N\/A",
                col8: "8900"
            })
        );
    });

    it("colorRows updates the rows with color accordingly", function () {
        var col = data.query.results.row[24].col4, row = data.query.results.row[24];  // "-8.18"
        app.colorRows(col, row);
        expect(row.className).toBe("nochange");

        col = data.query.results.row[0].col4;
        row = data.query.results.row[0];
        app.colorRows(col, row);
        expect(row.className).toBe("up");

        col = data.query.results.row[1].col4;
        row = data.query.results.row[1];
        app.colorRows(col, row);
        expect(row.className).toBe("down");
    });

    it("generateRandom generates a number between the values as parameters", function () {
        var result = app.generateRandom(0, 10);
        expect(result).not.toBeLessThan(0);
        expect(result).not.toBeGreaterThan(11);
    });

    it("getFormattedDate returns a function in USA-time: mm/dd/yy", function () {
        var date = new Date("03/30/2015");
        expect(app.getFormattedDate(date)).toBe("03/30/2015");
    });

    it("getFormattedTime function should return a string with hh:mm am/pm format", function () {
        var date = new Date(2015, 3, 3, 17, 7, 38, 0);
        expect(app.getFormattedTime(date)).toBe("5:07 pm");
    });

    it("generateTestData function should fill series.BCS", function () {
        var oldData = app.series["BCS"].length, newData;
        app.generateTestData();
        newData = app.series["BCS"].length;
        expect(newData).toBeGreaterThan(oldData);
    });

    it("loop should be undefined", function () {
        expect(app.loop()).toBe(undefined);
    });

    it("replaceTable should produce a different table than the previous one", function () {
        var oldData, newData;
        app.init();
        app.generateTestData();
        oldData = JSON.stringify(document.querySelector("table").innerHTML);
        app.generateTestData();
        app.replaceTable();
        newData = JSON.stringify(document.querySelector("table").innerHTML);

        expect(oldData).not.toBe(newData);
    });

});